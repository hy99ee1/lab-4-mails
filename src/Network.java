import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

interface NetworkService {
    public String fetchData() throws URISyntaxException, IOException ;
}

public class Network implements NetworkService {

    String link = "";
    Network(String link) {
        this.link = link;
    }
    @Override
    public String fetchData() throws URISyntaxException, IOException {
        URI uri = new URI(link);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(uri.toURL().openStream());
        byte[] buffer = new byte[1024];
        int readied = 0;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while ((readied = bufferedInputStream.read(buffer, 0, 1024)) != -1) {
            byteArrayOutputStream.write(buffer, 0, readied);
        }
        bufferedInputStream.close();
        String data = byteArrayOutputStream.toString();
        byteArrayOutputStream.close();
        return data;
    }

}
