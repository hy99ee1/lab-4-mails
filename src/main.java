import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.style.Styler;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

public class main {
    public static void main(String[] args) throws URISyntaxException, IOException {
        //Decision.startDecision();
        SpamParcer spamParcer = new SpamParcer(new Network("https://www.py4e.com/code3/mbox.txt"));
        spamParcer.startParcing();
        GraphicsCreator graphicsCreator = new GraphicsCreator(spamParcer.getParcedData());
        graphicsCreator.showGraphic();
    }
}
