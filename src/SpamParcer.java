import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

public class SpamParcer {

//    "https://www.py4e.com/code3/mbox.txt"
    private NetworkService network = null;
    ArrayList<Double> probabilities = new ArrayList<>();;
    HashMap<String, Integer> parcedData = new HashMap<>();;
    SpamParcer(NetworkService network) throws URISyntaxException, IOException {
        this.network = network;

    }



    public void startParcing() throws URISyntaxException, IOException {

        String[] strings = network.fetchData().split("\n");

        HashMap<String, ArrayList<Double>> confidenceByEmail = new HashMap<>();
        String currentEmail = "";
        for (String string : strings) {
            if (string.contains("X-DSPAM-Probability: ")) {
                String[] probability = string.split(":");
                if (probability.length != 2) {
                    continue;
                }
                probabilities.add(Double.parseDouble(probability[1]));
            }

            if (string.contains("From: ")) {
                String[] from = string.split(":");
                if (from.length != 2) {
                    continue;
                }
                currentEmail = from[1].strip();
                if (parcedData.containsKey(from[1].strip())) {
                    Integer msgCnt = parcedData.get(from[1].strip());
                    parcedData.put(from[1].strip(), msgCnt + 1);
                } else {
                    parcedData.put(from[1].strip(), 1);
                }
            }

            if (string.contains("X-DSPAM-Confidence: ")) {
                String[] c = string.split(":");
                if (c.length != 2) {
                    continue;
                }
                if (confidenceByEmail.containsKey(currentEmail)) {
                    ArrayList<Double> confidence = confidenceByEmail.get(currentEmail);
                    confidence.add(Double.valueOf(c[1]));
                    confidenceByEmail.put(currentEmail, confidence);
                } else {
                    ArrayList<Double> value = new ArrayList<>();
                    value.add(Double.valueOf(c[1]));
                    confidenceByEmail.put(currentEmail, value);
                }
            }
        }
        System.out.print("Probability: ");
        System.out.println(probabilities.stream().reduce(Double::sum).get() / probabilities.size());

        ArrayList<String> banned = new ArrayList<>();

        confidenceByEmail.forEach((k,v) -> {
            double v1 = v.stream().reduce(Double::sum).get() / v.size();
            if (v1 <= 0.8) {
                banned.add(k);
            }
        });

        System.out.print("Spammers: ");
        System.out.println(banned);

    }
    public HashMap<String, Integer> getParcedData() {
        return parcedData;
    }

}
