import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.style.Styler;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class GraphicsCreator {
    CategoryChart gist = null;
    ArrayList<String> x = new ArrayList<>();
    ArrayList<Double> y = new ArrayList<>();

    GraphicsCreator(HashMap<String, Integer> data) {

        gist = new CategoryChartBuilder().width(1700).height(500).title("Score").xAxisTitle("Abonents").yAxisTitle("Count").build();
        setDefaultGistStyle();


        data.forEach((k, v) -> {
            if (v > 50) {
                x.add(k);
                y.add((double) v);
            }
        });

        gist.addSeries("Histogram", x, y);
    }

    private void setDefaultGistStyle() {
        gist.getStyler().setLegendPosition(Styler.LegendPosition.InsideNE);
        gist.getStyler().setChartBackgroundColor(Color.orange);
        gist.getStyler().setPlotGridLinesColor(Color.BLACK);
        gist.getStyler().setPlotBackgroundColor(Color.GRAY);
        gist.getStyler().setPlotBorderColor(Color.BLACK);
        gist.getStyler().setSeriesColors(new Color[]{Color.DARK_GRAY});
    }

    public void showGraphic() {
        new SwingWrapper<>(gist).displayChart();
    }
}
